/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2014  Vishesh Handa <me@vhanda.in>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef TVSHOWFETCHJOB_H
#define TVSHOWFETCHJOB_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include "tvshow.h"
#include <tmdbqt/themoviedbapi.h>

namespace Jungle {

class TvShowFetchJob : public QObject
{
    Q_OBJECT
public:
    explicit TvShowFetchJob(TmdbQt::TheMovieDbApi* api, const QString& name, QObject* parent = 0);
    virtual ~TvShowFetchJob();

    TvShow result() { return m_show; }

signals:
    void result(TvShowFetchJob* job);

private slots:
    void slotResult(TmdbQt::TvSearchJob* job);
    void slotResult(TmdbQt::TvShowInfoJob* job);
    void slotNetworkReply(QNetworkReply* reply);

private:
    TmdbQt::TheMovieDbApi* m_api;
    QNetworkAccessManager m_network;
    QString m_name;

    TvShow m_show;
    QList<TvSeason> m_seasons;

    int m_pendingJobs;
};

}
#endif // TVSHOWFETCHJOB_H
